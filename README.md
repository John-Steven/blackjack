# Projekt: Blackjack (Fr/1, Kr)

Name & Praktikumstermin: John Beinecke, 5233474 (Fr/1, Kr)

---

### Inhaltsverzeichnis

- [Kurzbeschreibung inkl. Screenshot](#Kurzbeschreibung inklusive Screenshot)
- [Spielregeln](#Spielregeln)
- [Beschreibung des Projektaufbaus](#Beschreibung des Projektaufbaus)
  - [Abgabedateien](#Abgabedateien)
  - [Testdateien](#Testdateien)
  - [Aufbau der Anwendung](#Aufbau der Anwendung)
    - [Server](#Server)
    - [Client](#Client)
- [Dokumentation des implementierten WebAPIs](#Dokumentation des implementierten WebAPIs)
  - [Zur Implementierung](#Zur Implementierung)
  - [Zur Verwaltung](#Zur Verwaltung)
  - [Reaktion der Servers auf Clientnachrichten:](#Reaktion der Servers auf Clientnachrichten:)
- [Dokumentation des Interfaces](#Dokumentation des Interfaces)
  - [GameManageable](#GameManageable)
  - [CardManageable](#CardManageable)
  - [HandManageable](#HandManageable)
  - [PlayerManageable](#PlayerManageable)
  - [RewardManageable](#RewardManageable)
- [Technischer Anspruch (TA) und Umsetzung der Features](#Technischer Anspruch und Umsetzung der Features)
  - [Dokumentation der Features](#Dokumentation der Features)
    - [Bootstrap](#Bootstrap)
    - [Websockets](#Websockets)
    - [JSON-Datenformate](#JSON) 
- [Allgemeines](#Allgemeines)
  - [Visualisierung der Spielkarten](#Visualisierung der Spielkarten)
- [Quellennachweis](#Quellennachweis)

---
## Kurzbeschreibung inklusive Screenshot

> Mit dem Projekt "Blackjack" ist es dem Client möglich, das beliebte Glücksspiel
 [Blackjack](https://de.wikipedia.org/wiki/Black_Jack) zu spielen. Er kann ein Spiel
 für sich selbst oder bis zu drei Spieler erstellen und sein Glück über mehrere Runden
 versuchen. Dabei erhält er zu Start des Spiels, wenn die Lobby voll ist, ein bereits gefülltes Bankkonto und kann
 nun auf seinen eigenen Erfolg wetten und somit seinen Kontostand erhöhen.
 Sollte er jedoch irgendwann mal etwas übermütig gewesen sein und alles verlieren, 
 wird er aus dem Spiel entfernt.
 Sollte der Client jetzt kein neues Spiel erstellen wollen, kann er einem anderen Spiel
 beitreten und da sein Glück mit anderen Spielern versuchen.
 Sollte es mal passieren, dass ein Spieler die Lust verliert und das laufende Spiel
 verlässt, können die anderen Spieler unberuhigt weiterspielen und die Größe der Spiellobby
 wird für die nächste Runde entsprechend angepasst.


![Screenshot](Screenshot.png)

### Spielregeln
Zu Beginn des Spieles erhält jeder Spieler zwei Karten und der Dealer eine.
Nun entscheiden die Spieler nacheinander, ob Sie noch Karten ziehen wollen.
Dabei ist ihr Ziel mit ihrer Hand einen höheren Wert als der Dealer zu haben,
jedoch nicht dabei nicht über 21 zu kommen.

Wenn alle Spieler entschieden haben, zieht der Dealer solange Karten, bis er
über 16 kommt.

Nun wir schließlich entschieden, wer gewinnt:
- Ist der Dealer über 21 gewinnt jeder Spieler, der nicht über 21 ist.
- Ist der Spieler nicht über 21 und höher als der Dealer, gewinnt er.
- Ist der Spieler nicht höher als der Dealer oder über 21 verliert er.

## Beschreibung des Projektaufbaus

### Abgabedateien

Verlinkter Dateiname | Dateiart | LOC
---------------------|----------|-----
[App.java](/src/main/java/blackjack/App.java) | Java | 15
[GameManageable.java](/src/main/java/blackjack/game/GameManageable.java) | Java | 5
[CardManageable.java](/src/main/java/blackjack/game/CardManageable.java) | Java | 5
[HandManageable.java](/src/main/java/blackjack/game/HandManageable.java) | Java | 10
[PlayerManageable.java](/src/main/java/blackjack/game/PlayerManageable.java) | Java | 8
[RewardManageable.java](/src/main/java/blackjack/game/RewardManageable.java) | Java | 6
[Game.java](/src/main/java/blackjack/game/Game.java) | Java | 22
[GameSystem.java](/src/main/java/blackjack/game/GameSystem.java) | Java | 54
[CardSystem.java](/src/main/java/blackjack/game/CardSystem.java) | Java | 25
[HandSystem.java](/src/main/java/blackjack/game/HandSystem.java) | Java | 80
[PlayerSystem.java](/src/main/java/blackjack/game/PlayerSystem.java) | Java | 79
[RewardSystem.java](/src/main/java/blackjack/game/RewardSystem.java) | Java | 66
[Player.java](/src/main/java/blackjack/entities/Player.java) | Java | 10
[Card.java](/src/main/java/blackjack/entities/Card.java) | Java | 9
[GameService.java](/src/main/java/blackjack/services/GameService.java) | Java | 32
[SocketService.java](/src/main/java/blackjack/services/SocketService.java) | Java | 32
[SocketController.java](/src/main/java/blackjack/controller/SocketController.java) | Java | 47
[GameController.java](/src/main/java/blackjack/controller/GameController.java) | Java | 80
[HandController.java](/src/main/java/blackjack/controller/HandController.java) | Java | 59
[PlayerController.java](/src/main/java/blackjack/controller/PlayerController.java) | Java | 56
[RewardController](/src/main/java/blackjack/controller/RewardController.java) | Java | 30
[GameException.java](/src/main/java/blackjack/utils/GameException.java) | Java | 6
[JSONUtil.java](/src/main/java/blackjack/utils/JSONUtil.java) | Java | 39
[SessionUtil.java](/src/main/java/blackjack/utils/SessionUtil.java) | Java | 14
[GameState.java](/src/main/java/blackjack/utils/GameState.java) | Java | 2
[index.html](/src/main/resources/public/index.html) | HTML | 135
[main.js](/src/main/resources/public/main.js) | JavaScript | 189
[main.css](/src/main/resources/public/main.css) | CSS | 59

### Testdateien

Verlinkter Dateiname | Testart | Anzahl der Tests
---------------------|---------|-----------------
[CardSystemTest.java](/src/test/java/blackjack/game/CardSystemTest.java) | JUnit4 | 35
[GameSystemTest.java](/src/test/java/blackjack/game/GameSystemTest.java) | JUnit4 | 35
[HandSystemTest.java](/src/test/java/blackjack/game/HandSystemTest.java) | JUnit4 | 35
[PlayerSystemTest.java](/src/test/java/blackjack/game/PlayerSystemTest.java) | JUnit4 | 35
[RewardSystemTest.java](/src/test/java/blackjack/game/RewardSystemTest.java) | JUnit4 | 35
[GameService.java](/src/test/java/blackjack/services/GameServiceTest.java) | JUnit4 | 35

Die Tests werden wie folgt ausgeführt:

Durch das Ausführen des Befehls **gradle test** im Verzeichnis der README.

Hinweis zu den Tests: Es wird hauptsächlich die Spiellogik in den **System-Klassen**
getestet. Da diese Logik sehr stark miteinander verkettet ist, ist es in vielen Fällen
schwer bestimmte Tests unabhängig von dem Rest der Spiellogik zu testen. Daher wird
generell über die Simulation eines Spielesablaufs getestet. Da dieser zufällig ist,
müssen oft verschiedene Fälle geprüft werden.
Damit diese oft sehr komplexen Tests verständlich sind, habe ich sie entsprechend ihrer
Testfunktion benannt.

### Aufbau der Anwendung

>Im Folgende gebe ich grob, die Struktur meines Projektes wieder.
Für genauere Informationen über die Implementierung der Klassen
und einzelner Methoden, schauen Sie bitte in die Kommentare im Quellcode.

##### Grundlegende Struktur

![Structure](Package Overview.png)

#### Server
Das Projekt besteht auf der Serverseite aus 4 Paketen:

- game
- services
- controller
- utils

Der eigentliche Javalin-Server wird in [App.java](src/main/java/blackjack/App.java) gestartet.

---

#### game
>Das Paket *"game"* enthält die gekapselte Spiellogik von Blackjack.

Das eigentliche Blackjack-Spiel wird über die Klasse **"Game"** repräsentiert,
während alle Arten von Datenverarbeitung über die Klassen **"GameSystem, PlayerSystem
CardSystem, HandSystem und RewardSystem"** aufgeteilt sind.

Eine Instanz von **"Game"** ist jeweils mit einer unveränderlichen Instanz aller **"System-Klassen"**
verbunden und diese umgekehrt auch mit dem Spiel, wodurch die **"System-Klassen"** über das
Game miteinander interagieren können. Für eine Übersicht über die Funktionalitäten der *"System-Klassen"
siehe [Dokumentation des Interfaces](#Dokumentation des Interfaces).


---

#### services
> Das Paket **"services"** enthält die Klassen **"GameService und SocketService"**, welche aktive
Spiele und Websocket-Sessions verwalten.

Es bietet somit die eigentliche Grundlage
für den Multiplayer.

##### GameService
Enthält eine Liste aller aktiven Spiele und bietet für diese
die Basisfunktionalitäten ADD, GET und REMOVE.

##### SocketService
Enthält eine List aller aktiven Websocket-Sessions und eine Map aller
Websocket-Sessions, die sich in einem Spiel befinden, zu der entsprechenden
Game-ID. Für diese bietet sie die Basisfunktionalitäten ADD, GET und REMOVE.

---

#### controller
>Das Paket *"controller"* ist die Schnittstelle zwischen den Clienten
und der Spiellogik.

Hier werden Client-Messages für das Spiel verarbeitet
,dafür gesorgt, dass der Client bei Änderungen immer über den aktuellsten
Zusand informiert ist und zuletzt auch, dass die Daten der **"Service-Klassen"**
immer aktuell sind.

##### SocketController
Kontrolliert das Hinzufügen und Entfernen von Websockets und die Verarbeitung
der ankommenden Nachrichten und sendet sie an die entsprechende **"Controller-Klasse""**
weiter. Sie ist also die zentrale Routingstelle des Servers.

##### GameController, PlayerController, HandController, RewardController
Kontrolliert die Verarbeitung der Clientnachrichten für die Spiellogik des entsprechenden
Bereiches und das Versenden der Servernachrichten als Reaktion auf diese.



--- 

#### utils
> Das Paket **"utils"** enthält verschiedene Utility-Klassen, die von allen
anderen Klassen benutzt werden.

---

#### Client

Auf der Clientseite ist das Projekt in [HTML](), [JavaScript]() und [CSS]() aufgeteilt.

Das JavaScript kümmert sich hierbei um die Verarbeitung der Servernachrichten, das Versenden
von Clientnachrichten und das entsprechende aktualisieren des GUI.


## Dokumentation des implementierten WebAPIs

Im Folgenden gehe ich auf den Ablauf der Kommunikation zwischen Client und Server ein.
Für genauere Informationen über die Implementierung der Klassen
und einzelner Methoden, schauen Sie bitte in die Kommentare im Quellcode.
Der Datenaustausch zwischen Server und Client findet über Websockets statt.

Die Nachrichten werden im JSON-Format versendet und folgen auf beiden Seiten
der folgenden Struktur:

##### JSON-FORMAT:
            
    {
    
    type: <action identifier>,
    
    content: <msg_data>
    
    }

Über den action identifier entscheidet der Empfänger wie die Nachricht verarbeitet werden soll,
während der content, die eigentlich zu verarbeitenden Daten enthält.

Auf beiden Seiten findet das Routing der Nachrichten zentral statt.

Auf der Clientseite werden alle Nachrichten in **"main.js"** als Reaktion auf die Interaktion des
Users mit der GUI versendet (eine Ausnahme siehe [RewardController.java]()(Zeile 39-66))
Servernachrichten kommen in **"main.js"** an und werden dort entsprechend für die GUI verarbeitet.

Auf der Serverseite kommen die Nachrichten im *"SocketController" an und werden
dort an die anderen **"Controller-Klassen"** weitergeleitet. Dort werden die Daten für die
Spiellogik verarbeitet und falls nötig, werden neue Servernachrichten an die relevanten
Clients geschickt.

Anmerkung:
Das Versenden einer Nachricht im Server wird über die Funktion "sendStringByFuture(String text)"
gemacht, da ohne die Nutzung von Futures ein Fehler im Jetty-Server auftritt, welcher anscheinend
mit einer überlaufenden Queue für Websocket-Messages zusammenhängt.

--- 
#### Zur Implementierung:

- Server:
  - neue Verbindung : Wird im SocketService zu der Liste aller Verbindungen hinzugefügt
  - neue Nachricht : Wird vom SocketController an andere Controller weitergeleitet und dort verarbeitet
  - Verbindungsabbruch : Wird im SocketService aus der Liste aller Verbindungen gelöscht
  
- Client:
  - neue Verbindung : Legt beim Öffnen der Seite einen neuen Websocket zum Server an
  - neue Nachricht : Verarbeitet die Nachricht für die GUI
  - Verbindungsabbruch : Bricht ab und falls das Tab geschlossen wird und die Session in einem Spiel war,
  wird noch ein "leaveGame" für diesen Spieler an den Server geschickt

---
#### Zur Verwaltung:
 Die Verwaltung der Websockets findet in der SocketService-Klasse statt, wo eine Liste aller aktiven
 Sessions und eine Map aller aktiven Sessions, die in einem Spiel sind, zu der entsprechenden 
 Spiel-ID gespeichert wird.
 
#### Reaktion der Servers auf Clientnachrichten:

Für eine detailierte Dokumentation der entsprechenden Funktionen, schauen Sie in den Quellcode
der **"Controller-Klassen"**.

 - newGame: Erstellt ein neues Spiel mit einem Spieler
       
       Parameter:
       - game_name: Name des Spiels
       - username: Name des Spielers
       - max_players: Anzahl der Spieler
 
       Updates:
       - gamesAndPlayerData
       - updateAllGames
       - updateIfIsReady
       - updateHandInfo
       - updatePlayerInfo
   
 - joinGame: Fügt einen Spieler einem existierenden Spiel hinzu
        
       Parameter:
       - game_uuid: ID des Spiels
       - username: Name des Spielers
        
       Updates: 
       - gameAndPlayerData
       - updateAllGames
       - updateIfIsReady
       - updateHandInfo
       - updatePlayerInfo
   
 - bet: Setzt eine Wette für einen Spieler
       
       Parameter:
       - game_uuid: ID des Spiels
       - player_uuid: ID des Spielers
       
       Update:
       - updatePlayerInfo
       - updatePlayerBank
       - updateHandInfo
       - updateIfIsFinished
   
 - draw: Zieht eine Karte für einen Spieler
 
        Parameter:
        - game_uuid: ID des Spiels
        - player_uuid: ID des Spielers
        
        Update:
 
       - updateHandInfo
       - updatePlayerInfo
       - updateIfIsFinished
   
 - stand: Gibt den Zug für einen Spieler weiter
 
        Parameter:
        - game_uuid: ID des Spiels
        - player_uuid: ID des Spielers
        
        Update:
       - updateHandInfo
       - updatePlayerInfo
       - updateIfIsFinished
   
 - playerBank: Holt die Bank für den Spieler
 
        Parameter:
        - game_uuid: ID des Spiels
        - player_uuid: ID des Spielers
        
        Update:
       - updatePlayerBank
 
 - resetGame: Startet eine neue Runde
 
        Parameter:
        - game_uuid: ID des Spiels
        
        Update:
       - resetGame
       - updateHandInfo
       - updateIfIsReady
       - updatePlayerInfo
   
 - leaveGame
 
        Parameter:
        - game_uuid: ID des Spiels
        - player_uuid: ID des Spielers
        
        Update:
       - updateHandInfo
       - updateIfIsFinished
       - updatePlayerInfo
       - updateAllGames
 

## Dokumentation des Interfaces

Im Folgenden gehe ich auf die verschiedenen Interfaces für die **"System-Klassen"** ein,
welche ihre Grundfunktionalitäten definieren.

### GameManageable
    
    void changeGameState(GameState gameState);
    Hauptfunktion zur Kontrolle des Spielzustandes und der Definierung der Reaktionen auf seine Änderungen.
   

### CardManageable

    Card drawCard()
    Zieht und entfernt eine Karte aus dem Deck und gibt diese zurück.

### HandManageable

    int getHandValue(List<Card> hand)
    Berechnet den Wert einer Hand, nach den Regeln von Blackjack.
    
    void playerDraw(UUID playerUUID)
    Zieht eine Karte für die Hand eines Spielers, der über die playerUUID identifiziert wird.
    
    void playerStand(UUID playerUUID)
    Gibt an, dass dieser Spieler keine Karten mehr ziehen möchte.
    
    void dealerDraw()
    Zieht eine Karte für die Hand des Dealers.

### PlayerManageable
    
    
    void addPlayer(Player player)
    Fügt einen Spieler zu einem Blackjack-Spiel hinzu.
    
    void removePlayer(UUID playerUUID)
    Entfernt einen Spieler anhand seiner playerUUID aus dem Spiel.
    
    Player getCurrPlayer()
    Gibt den Spieler zurück, welcher gerade am Zug ist.

### RewardManageable

   
    void bet(UUID playerUUID, int cash)
    Verarbeitet den Wetteinsatz (cash) eines Spielers anhand seiner playerUUID
    
    void processRewards()
    Verarbeitet für alle Spieler ihren Gewinn/Verlust.
    

## Technischer Anspruch und Umsetzung der Features

Ich habe folgende Features verwendet. Die verlinkte Datei zeigt beispielhaft den Einsatz dieses Features in den angegebenen Zeilen im Quellcode.

1. Nutzung einer externen Bibliothek für das Frontend aus HTML/CSS/JavaScript, Bootstrap in [index.html](/src/main/resources/public/index.html)(Bootstrap-Modal:84-155),
und JQuery in [main.js](/src/main/resources/public/main.js)(Die ganze Datei)
2. WebSockets, [SocketController.java](/src/main/java/blackjack/controller/SocketController.java) (Die ganze Datei)
,[SocketService.java](/src/main/java/blackjack/services/SocketService.java) (20-21)
3. JSON-Datenformate, [JSONUtil.java](/src/main/java/blackjack/utils/JSONUtil.java) (Die ganze Datei)
4. Streams, [SessionUtil.java](/src/main/java/blackjack/utils/SessionUtil.java) (17-31),
[PlayerController.java](/src/main/java/blackjack/controller/PlayerController.java) (94-101),
[HandController.java](/src/main/java/blackjack/controller/HandController.java) (77-87),
[SocketService.java](/src/main/java/blackjack/services/SocketService.java) (48-51),


### Dokumentation der Features
#### Bootstrap
Das Grundgerüst der Website ist mit dem [Grid-System](https://getbootstrap.com/docs/4.0/layout/grid/) von Bootstrap gebaut. Hierbei besteht die
Website eigentlich nur aus rows (Zeilen) und darin geschachtelten cols (Spalten), in denen dann
schließlich die GUI-Elemente liegen.
Weiterhin nutze ich ein [Bootstrap-Modal](https://getbootstrap.com/docs/4.0/components/modal/) für das Create-Game/Join-Game Menü.
Zuletzt benutze ich Bootstrap-Basisfeatures, wie Knöpfe oder Radio Buttons.

##### Visualisierung des Grid-Systems
![](Bootstrap-Pic.png)

#### WebSockets
Die Dokumenation der Websockets ist unter [Dokumentation des implementierten WebAPIs](#Dokumentation des implementierten WebAPIs) zu finden.
#### JSON
Für die Kommunikation über die Websockets werden die Daten in JSON formatiert.
Die Struktur dieser JSON-Dateien ist in [Dokumentation des implementierten WebAPIs](#Dokumentation des implementierten WebAPIs) definiert.

Auf der Seite des Clients ist das formatieren durch die JavaScript's [JSON Objects](https://www.w3schools.com/js/js_json_objects.asp)
ermöglicht, während zum interpretieren von Servernachrichten JSON.parse() verwendet wird.

Auf der Seite des Servers verwendet ich die JSON-API [Jackson](https://en.wikipedia.org/wiki/Jackson_(API).
In der Klasse **"JSONUtil"** baue ich die für den Client relevanten Daten in einen JSON-String in dem
verwendeten Format. Dies passiert über die Methoden "toJson()" oder "toJsonArray", je nachdem,
ob der Client durch die erhaltenen Daten iterieren muss.
Für das interpretieren von Clientnacrichten benutze ich die JSONUtil Methode parseJson.

Die eben genannten für den Client relevante Daten, werden bereits in den Controllern zu JSON gewandelt
und als einzelne Node oder Liste von Nodes durch JSONUtil nurnoch an den content angehängt.

#### Streams
Für genaueres über die Streams, schauen Sie in den oben verlinkten Dateien in den entsprechenden Zeilen.


## Allgemeines
####Visualisierung der Spielkarten
Die Spielkarten werden von der Klasse [Card](/src/main/java/blackjack/entities/Card.java) repräsentiert,
in welcher zwei Integer für den Wert und die Farbe gespeichert werden.

Diese Daten werden im HandController in der Methode renderHand(List<Card> hand)
weiterverarbeitet. Dort wird das HTML für die Karten erstellt, wobei die Verknüpfung von
Farbe + Wert bestimmt, welche Bilddatei aus dem [img-Ordner](/src/main/resources/public/img) geladen werden soll.

     String imgName = "" + card.getType() + card.getNumber();
     
Zusätzlich wird hier, abhängig von der Größe der Hand, die Rotation der Karten
berechnet, damit alle Karten gleichmäßig ausgefächert sind.

    int rotateDegree = 30;
    int startDegree = (-rotateDegree/2)*(hand.size()-1);
    
    for(Card card:hand){

        ...HTML-BUILDING...

        startDegree = startDegree + rotateDegree;
        
    }



## Quellennachweis

* [Holz-Image](http://de.psdexport.com/psdfile-20-dark-wood-background-textures-177034.htm)
* [Chip-Image](https://www.svgrepo.com/svg/4886/poker-chip)
* [Karten-Images](http://acbl.mybigcommerce.com/52-playing-cards/)
* [Icon](https://www.favicon.cc/?action=icon&file_id=734494)
* [Bootstrap](https://getbootstrap.com/)
* [JQuery](https://jquery.com/)
* [Project Lombok](https://projectlombok.org/)


