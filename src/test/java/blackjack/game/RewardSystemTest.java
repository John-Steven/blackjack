package blackjack.game;

import blackjack.TestUtils;
import blackjack.entities.Player;
import blackjack.utils.GameException;
import blackjack.utils.GameState;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.UUID;

import static blackjack.TestUtils.finishGame;
import static blackjack.TestUtils.getOFFgame;
import static org.junit.Assert.*;

public class RewardSystemTest {

    private Game game;
    private Player p1;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Before
    public void setup(){
        game = getOFFgame();
        p1 = new Player(UUID.randomUUID(),"p1");
        game.getPlayerSystem().addPlayer(p1);
    }

    @Test
    public void playerBankHas5000AtStart(){
        assertEquals(
            Integer.valueOf(5000),
            game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID()));
    }

    @Test
    public void playerBetHas0AtStart(){
        assertEquals(
            Integer.valueOf(0),
            game.getRewardSystem().getPlayerBets().get(p1.getPlayerUUID()));
    }

    @Test
    public void playerBetTest(){
        game.getGameSystem().changeGameState(GameState.BET);
        game.getRewardSystem().bet(p1.getPlayerUUID(),500);

        if(!game.getGameSystem().isFinished()) {
            assertEquals(
                Integer.valueOf(4500),
                game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID()));
            assertEquals(
                Integer.valueOf(500),
                game.getRewardSystem().getPlayerBets().get(p1.getPlayerUUID()));
        }
        assertTrue(game.getRewardSystem().allHaveBet());
    }

    @Test
    public void playerCantBetTwiceTest(){
        Player p2 = new Player(UUID.randomUUID(),"p2");
        game.getPlayerSystem().addPlayer(p2);

        game.getGameSystem().changeGameState(GameState.BET);

        game.getRewardSystem().bet(p1.getPlayerUUID(),500);

        exceptionRule.expect(GameException.class);
        exceptionRule.expectMessage("Player has already bet!");

        game.getRewardSystem().bet(p1.getPlayerUUID(),500);

    }

    @Test
    public void multiplePlayerBetTest(){
        Player p2 = new Player(UUID.randomUUID(),"p2");
        game.getPlayerSystem().addPlayer(p2);

        game.getGameSystem().changeGameState(GameState.BET);

        game.getRewardSystem().bet(p1.getPlayerUUID(),500);

        assertEquals(
            Integer.valueOf(4500),
            game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID()));
        assertEquals(
            Integer.valueOf(500),
            game.getRewardSystem().getPlayerBets().get(p1.getPlayerUUID()));
        assertFalse(game.getRewardSystem().allHaveBet());

        game.getRewardSystem().bet(p2.getPlayerUUID(),1000);

        assertEquals(
            Integer.valueOf(4000),
            game.getRewardSystem().getPlayerBanks().get(p2.getPlayerUUID()));
        assertEquals(
            Integer.valueOf(1000),
            game.getRewardSystem().getPlayerBets().get(p2.getPlayerUUID()));
        assertTrue(game.getRewardSystem().allHaveBet());
    }

    @Test
    public void processRewardsTest1(){
        Game game = new Game(UUID.randomUUID(),1,"testGame");
        Player p1 = new Player(UUID.randomUUID(),"Test");
        game.getPlayerSystem().addPlayer(p1);
        game.getGameSystem().changeGameState(GameState.BET);
        game.getRewardSystem().bet(p1.getPlayerUUID(),500);

        TestUtils.finishGame(game);

        assertTrue(
            game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID()).equals(5000)
        || game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID()).equals(5500)
        ||  game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID()).equals(4500));
    }

    @Test
    public void processRewardsTest2(){
        Game game = new Game(UUID.randomUUID(),2,"testGame");
        Player p1 = new Player(UUID.randomUUID(),"testPlayer1");
        Player p2 = new Player(UUID.randomUUID(),"testPlayer2");
        game.getPlayerSystem().addPlayer(p1);
        game.getPlayerSystem().addPlayer(p2);
        game.getGameSystem().changeGameState(GameState.BET);
        game.getRewardSystem().bet(p1.getPlayerUUID(),500);
        game.getRewardSystem().bet(p2.getPlayerUUID(),1500);

        finishGame(game);

        assertTrue(
            game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID()).equals(5000)
                || game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID()).equals(5500)
                ||  game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID()).equals(4500));
        assertTrue(
            game.getRewardSystem().getPlayerBanks().get(p2.getPlayerUUID()).equals(5000)
                || game.getRewardSystem().getPlayerBanks().get(p2.getPlayerUUID()).equals(6500)
                ||  game.getRewardSystem().getPlayerBanks().get(p2.getPlayerUUID()).equals(3500));
    }

    @Test
    public void gameStartsWhenAllHaveBet(){

        Player p2 = new Player(UUID.randomUUID(),"p2");
        game.getPlayerSystem().addPlayer(p2);

        game.getGameSystem().changeGameState(GameState.BET);

        game.getRewardSystem().bet(p1.getPlayerUUID(),500);
        game.getRewardSystem().bet(p2.getPlayerUUID(),1500);

        assertTrue(
            game.getGameSystem().getGameState().equals(GameState.PLAYERS_CHOOSING)
            || game.getGameSystem().getGameState().equals(GameState.FINISHED));
    }

    @Test
    public void playerCantBetHigherThanBank(){
        game.getGameSystem().changeGameState(GameState.BET);

        game.getRewardSystem().bet(p1.getPlayerUUID(),5001);

        assertEquals(game.getRewardSystem().getPlayerBets().get(p1.getPlayerUUID()),Integer.valueOf(0));
    }

    @Test
    public void playerBankStaysAfterReset(){
        Game game = new Game(UUID.randomUUID(),1,"testGame");
        Player p1 = new Player(UUID.randomUUID(),"Test");
        game.getPlayerSystem().addPlayer(p1);
        game.getGameSystem().changeGameState(GameState.BET);
        game.getRewardSystem().bet(p1.getPlayerUUID(),500);

        TestUtils.finishGame(game);

        int playerBankBefore = game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID());

        game.getRewardSystem().init();

        int playerBankAfter = game.getRewardSystem().getPlayerBanks().get(p1.getPlayerUUID());

        assertEquals(playerBankBefore,playerBankAfter);
    }

    @Test
    public void initTest(){
        assertTrue(game.getRewardSystem().getPlayerBets().values().stream().allMatch(bet->bet==0));
    }

}
