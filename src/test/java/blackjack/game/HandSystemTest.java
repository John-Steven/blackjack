package blackjack.game;

import blackjack.entities.Card;
import blackjack.entities.Player;
import blackjack.utils.GameState;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static blackjack.TestUtils.getOFFgame;
import static org.junit.Assert.*;
public class HandSystemTest {

    private Game game;
    private Player p1,p2;

    @Before
    public void setup(){
        game = getOFFgame();
        p1 = new Player(UUID.randomUUID(),"p1");
        p2 = new Player(UUID.randomUUID(),"p2");
        game.getPlayerSystem().addPlayer(p1);
        game.getPlayerSystem().addPlayer(p2);

    }

    @Test
    public void handValueTest1(){
        assertEquals(0,game.getHandSystem().getHandValue(List.of()));
    }

    @Test
    public void handValueTest2(){

        List<Card> cards = List.of(
            new Card(2,1),
            new Card(3,1),
            new Card(4,1),
            new Card(5,1),
            new Card(6,1),
            new Card(7,1),
            new Card(8,1),
            new Card(9,1),
            new Card(10,1),
            new Card(11,1),
            new Card(12,1),
            new Card(13,1)
        );

        assertEquals(84, game.getHandSystem().getHandValue(cards));
    }

    @Test
    public void handValueTest3(){

        List<Card> cards = List.of(
            new Card(1,1),
            new Card(10,1)
        );

        assertEquals(21, game.getHandSystem().getHandValue(cards));
    }

    @Test
    public void handValueTest4(){

        List<Card> cards = List.of(
            new Card(10,1),
            new Card(10,1),
            new Card(1,1)
        );

        assertEquals(21, game.getHandSystem().getHandValue(cards));
    }

    @Test
    public void handValueTest5(){
        List<Card> cards = new ArrayList<>();
        cards.add(new Card(10,1));
        cards.add(new Card(1,1));

        assertEquals(21, game.getHandSystem().getHandValue(cards));

        cards.add(new Card(10,1));

        assertEquals(21, game.getHandSystem().getHandValue(cards));
    }

    @Test
    public void allPlayersHave2CardsAfterStart(){
        game.getGameSystem().changeGameState(GameState.STARTED);

        assertEquals(2, game.getHandSystem().getPlayerHand(p1.getPlayerUUID()).size());
        assertEquals(2, game.getHandSystem().getPlayerHand(p2.getPlayerUUID()).size());
    }

    @Test
    public void dealerHasOneCardAfterStart(){
        game.getGameSystem().changeGameState(GameState.STARTED);

        assertEquals(1,game.getHandSystem().getDealerHand().size());
    }

    @Test
    public void goesToNextPlayerWhenOverOrOn21() {

        Player p3 = new Player(UUID.randomUUID(),"p3");
        game.getPlayerSystem().addPlayer(p3);

        game.getRewardSystem().bet(p1.getPlayerUUID(),500);
        game.getRewardSystem().bet(p2.getPlayerUUID(),500);
        game.getRewardSystem().bet(p3.getPlayerUUID(),500);

        while (!game.getHandSystem().isOverOrOn21(p1.getPlayerUUID())) {
            game.getHandSystem().playerDraw(p1.getPlayerUUID());
        }
        System.out.println(game.getHandSystem().getHandValue(game.getHandSystem().getPlayerHand(p1.getPlayerUUID())));


        if (!game.getHandSystem().isOverOrOn21(p2.getPlayerUUID())) {
            assertEquals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID(), p2.getPlayerUUID());
        } else {
            assertNotEquals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID(), p2.getPlayerUUID());
        }
    }

    @Test
    public void goesToNextPlayerWhenStand(){
        Player p3 = new Player(UUID.randomUUID(),"p3");
        game.getPlayerSystem().addPlayer(p3);

        game.getRewardSystem().bet(p1.getPlayerUUID(),500);
        game.getRewardSystem().bet(p2.getPlayerUUID(),500);
        game.getRewardSystem().bet(p3.getPlayerUUID(),500);

        if(!game.getHandSystem().isOverOrOn21(p1.getPlayerUUID())){
            game.getHandSystem().playerStand(p1.getPlayerUUID());
        }

        if(!game.getHandSystem().isOverOrOn21(p2.getPlayerUUID())){
            assertEquals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID(),p2.getPlayerUUID());
        }
        else{
            assertNotEquals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID(),p2.getPlayerUUID());
        }
    }

    @Test
    public void dealerIsNeverUnder17AfterDealerDraw(){
        game.getGameSystem().changeGameState(GameState.STARTED);

        if(!game.getHandSystem().isOverOrOn21(p1.getPlayerUUID())){
            game.getHandSystem().playerStand(p1.getPlayerUUID());
        }
        if(!game.getHandSystem().isOverOrOn21(p2.getPlayerUUID())){
            game.getHandSystem().playerStand(p2.getPlayerUUID());
        }

        assertTrue(game.getHandSystem().getDealerValue()>16);
    }

    @Test
    public void initTest(){
        assertTrue(game.getHandSystem().getPlayerHands().values().stream()
            .allMatch(hand->hand.size()==0));
    }


}
