package blackjack.game;

import blackjack.entities.Card;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.stream.IntStream;

import static blackjack.TestUtils.getOFFgame;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CardSystemTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void drawingCardTest1(){
        Game game = getOFFgame();
        Card card = game.getCardSystem().drawCard();

        assertEquals(
            game.getCardSystem().getAllCards().stream().filter(c->c.equals(card)).count(),
            5);
    }

    @Test
    public void drawingCardTest2(){
        Game game = getOFFgame();
        Card card1 = game.getCardSystem().drawCard();
        Card card2 = game.getCardSystem().drawCard();

        if(card1.equals(card2)){
            assertEquals(
                game.getCardSystem().getAllCards().stream().filter(c->c.equals(card1)).count(),
                4);
        }
        else{
            assertEquals(
                game.getCardSystem().getAllCards().stream().filter(c->c.equals(card1)).count(),
                5);
            assertEquals(
                game.getCardSystem().getAllCards().stream().filter(c->c.equals(card2)).count(),
                5);
        }
    }

    @Test
    public void sixOfEachCardsInDeck(){
        Game game = getOFFgame();

        IntStream.range(0,6).forEach(n->IntStream.rangeClosed(1,4)
            .forEach(i-> IntStream.rangeClosed(1,13)
                .forEach(j-> game.getCardSystem().getAllCards().remove(new Card(j,i)))));

        assertEquals(0, game.getCardSystem().getAllCards().size());
    }

    @Test
    public void drawing313RefillsDeck(){
        Game game = getOFFgame();

        IntStream.range(0,313).forEach(i-> game.getCardSystem().drawCard());

        assertEquals(311, game.getCardSystem().getAllCards().size());
    }

    @Test
    public void deckDoesNotContainIllegalCards(){

        Game game = getOFFgame();

        assertTrue(
            game.getCardSystem()
                .getAllCards()
                .stream()
                .allMatch(card->
                    card.getNumber()>=1 && card.getNumber()<=13
                        && card.getType()>=1 && card.getType()<=4));
    }

    @Test
    public void initTest(){
        Game game = getOFFgame();
        assertEquals(312,game.getCardSystem().getAllCards().size());
    }
}
