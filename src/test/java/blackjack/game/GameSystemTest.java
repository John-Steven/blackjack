package blackjack.game;

import blackjack.TestUtils;
import blackjack.entities.Player;
import blackjack.utils.GameState;
import org.junit.Test;

import java.util.UUID;

import static blackjack.TestUtils.getOFFgame;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GameSystemTest {

    @Test
    public void stateFlowTest(){
        Game game = getOFFgame();
        Player p1 = new Player(UUID.randomUUID(),"p1");
        game.getPlayerSystem().addPlayer(p1);

        game.getRewardSystem().bet(p1.getPlayerUUID(),500);

        if(game.getHandSystem().isOverOrOn21(p1.getPlayerUUID())){
            assertEquals(game.getGameSystem().getGameState(), GameState.FINISHED);
        }
        else{
            assertEquals(game.getGameSystem().getGameState(), GameState.PLAYERS_CHOOSING);
        }
    }

    @Test
    public void gameStaysInPlayerChoosing(){
        Game game = getOFFgame();
        Player p1 = new Player(UUID.randomUUID(),"p1");
        Player p2 = new Player(UUID.randomUUID(),"p2");
        game.getPlayerSystem().addPlayer(p1);
        game.getPlayerSystem().addPlayer(p2);
        game.getRewardSystem().bet(p1.getPlayerUUID(),500);
        game.getRewardSystem().bet(p2.getPlayerUUID(),1500);

        if(!game.getGameSystem().getGameState().equals(GameState.FINISHED)){
            game.getHandSystem().playerStand(p1.getPlayerUUID());
            assertEquals(
                game.getGameSystem().getGameState(),
                GameState.PLAYERS_CHOOSING);
        }
    }

    @Test
    public void isFinishedTest(){

        UUID playerUUID = UUID.randomUUID();
        Player p1 = new Player(playerUUID,"testPlayer");
        Game game = new Game(UUID.randomUUID(),1,"testGame");
        game.getPlayerSystem().addPlayer(p1);

        game.getRewardSystem().bet(playerUUID,500);

        TestUtils.finishGame(game);

        assertTrue(game.getGameSystem().isFinished());
    }

    @Test
    public void initTest(){
        Game game = getOFFgame();
        assertEquals(game.getGameSystem().getGameState(), GameState.OFF);
    }

    @Test
    public void resetGameTest(){
        UUID playerUUID = UUID.randomUUID();
        Player p1 = new Player(playerUUID,"testPlayer");
        Game game = new Game(UUID.randomUUID(),1,"testGame");
        game.getPlayerSystem().addPlayer(p1);

        game.getRewardSystem().bet(playerUUID,500);

        TestUtils.finishGame(game);

        game.getGameSystem().resetGame();

        assertEquals(
            game.getGameSystem().getGameState(),
            GameState.BET);
    }
}
