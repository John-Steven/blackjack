package blackjack.game;

import blackjack.entities.Player;
import blackjack.utils.GameException;
import blackjack.utils.GameState;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.UUID;

import static blackjack.TestUtils.finishGame;
import static blackjack.TestUtils.getOFFgame;
import static org.junit.Assert.*;

public class PlayerSystemTest {

    private Player p1;
    private Game game;

    @Before
    public void setup(){
        game = getOFFgame();
        UUID playerUUID = UUID.randomUUID();
        p1 = new Player(playerUUID,"testPlayer");
        game.getPlayerSystem().addPlayer(p1);
    }


    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void addingPlayerTest1(){
        assertTrue(game.getPlayerSystem().getPlayers().contains(p1));
    }

    @Test
    public void addingPlayerTest2(){
        Player p2 = new Player(UUID.randomUUID(),"testPlayer");
        game.getPlayerSystem().addPlayer(p2);

        assertTrue(game.getPlayerSystem().getPlayers().containsAll(List.of(p1,p2)));
    }

    @Test
    public void addingPlayerTest3(){
        exceptionRule.expect(GameException.class);
        exceptionRule.expectMessage("Player can not be added!");

        game.getPlayerSystem().addPlayer(p1);
    }

    @Test
    public void removingPlayerTest1(){

        game.getPlayerSystem().removePlayer(p1.getPlayerUUID());

        assertFalse(game.getPlayerSystem().getPlayers().contains(p1));
    }

    @Test
    public void removingPlayerTest2(){
        Player p2 = new Player(UUID.randomUUID(),"testPlayer2");
        game.getPlayerSystem().addPlayer(p2);

        game.getPlayerSystem().removePlayer(p1.getPlayerUUID());
        game.getPlayerSystem().removePlayer(p2.getPlayerUUID());

        assertFalse(game.getPlayerSystem().getPlayers().containsAll(List.of(p1,p2)));
    }

    @Test
    public void removeSynchronizedTest(){
        Player p2 = new Player(UUID.randomUUID(),"testPlayer2");
        game.getPlayerSystem().addPlayer(p2);

        game.getPlayerSystem().removePlayer(p2.getPlayerUUID());

        assertFalse(
            game.getRewardSystem().getPlayerBanks().containsKey(p2.getPlayerUUID())
        || game.getRewardSystem().getPlayerBets().containsKey(p2.getPlayerUUID()));

    }

    @Test
    public void gettingTest(){
        assertEquals(p1,game.getPlayerSystem().getPlayer(p1.getPlayerUUID()));
    }

    @Test
    public void gettingNoneExistentPlayer(){
        UUID playerUUID = UUID.randomUUID();

        exceptionRule.expect(GameException.class);
        exceptionRule.expectMessage("Player does not exist!");

        game.getPlayerSystem().getPlayer(playerUUID);
    }

    @Test
    public void removingNoneExistentPlayer(){
        UUID playerUUID = UUID.randomUUID();

        exceptionRule.expect(GameException.class);
        exceptionRule.expectMessage("Player does not exist!");

        game.getPlayerSystem().removePlayer(playerUUID);
    }

    @Test
    public void currPlayerUpdatesAfterNextPlayer(){
        Player p2 = new Player(UUID.randomUUID(),"TestPlayer2");

        game.getPlayerSystem().addPlayer(p2);

        game.getGameSystem().changeGameState(GameState.STARTED);

        assertEquals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID(), p1.getPlayerUUID());
        game.getPlayerSystem().nextPlayer();
        assertEquals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID(),p2.getPlayerUUID());
    }

    @Test
    public void currPlayerUpdatesAfterPlayerLeaves(){
        Player p2 = new Player(UUID.randomUUID(),"TestPlayer2");

        game.getPlayerSystem().addPlayer(p2);

        game.getGameSystem().changeGameState(GameState.STARTED);

        assertEquals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID(), p1.getPlayerUUID());
        game.getPlayerSystem().removePlayer(p1.getPlayerUUID());
        assertEquals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID(),p2.getPlayerUUID());

    }

    @Test
    public void noPlayerIsCurrPlayerWhenGameNotStarted(){
        assertFalse(game.getPlayerSystem().getPlayers().stream().anyMatch(player->
            player.getPlayerUUID().equals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID())));
    }

    @Test
    public void noPlayerIsCurrPlayerWhenGameFinished(){
        Player tmpPlayer = new Player(UUID.randomUUID(),"testPlayer");
        Game tmpGame = new Game(UUID.randomUUID(),1,"testGame");
        tmpGame.getPlayerSystem().addPlayer(tmpPlayer);
        tmpGame.getRewardSystem().bet(tmpPlayer.getPlayerUUID(),500);

        finishGame(tmpGame);

        assertFalse(game.getPlayerSystem().getPlayers().stream().anyMatch(player->
            player.getPlayerUUID().equals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID())));

    }

    @Test
    public void maxPlayerTest1(){
        Player p2 = new Player(UUID.randomUUID(),"TestPlayer2");
        Player p3 = new Player(UUID.randomUUID(),"TestPlayer3");
        Player p4 = new Player(UUID.randomUUID(),"TestPlayer4");

        game.getPlayerSystem().addPlayer(p2);
        game.getPlayerSystem().addPlayer(p3);

        exceptionRule.expect(GameException.class);
        exceptionRule.expectMessage("Player can not be added!");

        game.getPlayerSystem().addPlayer(p4);
    }

    @Test
    public void maxPlayerTest2(){
        exceptionRule.expect(GameException.class);
        exceptionRule.expectMessage("Illegal maxPlayers value!");

        new Game(UUID.randomUUID(),4,"testGame");
    }

    @Test
    public void maxPlayerTest3(){
        exceptionRule.expect(GameException.class);
        exceptionRule.expectMessage("Illegal maxPlayers value!");

        new Game(UUID.randomUUID(),-1,"testGame");
    }

    @Test
    public void isReadyTest(){
        Player p2 = new Player(UUID.randomUUID(),"TestPlayer2");
        Player p3 = new Player(UUID.randomUUID(),"TestPlayer3");

        game.getPlayerSystem().addPlayer(p2);
        game.getPlayerSystem().addPlayer(p3);

        assertTrue(game.getPlayerSystem().isReady());
    }

    @Test
    public void initTest(){
        List<Player> playersBefore = game.getPlayerSystem().getPlayers();

        game.getPlayerSystem().init();

        List<Player> playersAfter = game.getPlayerSystem().getPlayers();

        assertEquals(playersBefore,playersAfter);
    }



    @Test
    public void synchronizedTest1(){
        assertTrue(game.getPlayerSystem().getPlayers().stream().allMatch(player->
            game.getHandSystem().getPlayerHands().keySet().contains(player.getPlayerUUID())
                && game.getRewardSystem().getPlayerBets().keySet().contains(player.getPlayerUUID())
                && game.getRewardSystem().getPlayerBanks().keySet().contains(player.getPlayerUUID())));
    }

    @Test
    public void synchronizedTest2(){
        Player p2 = new Player(UUID.randomUUID(),"TestPlayer2");
        Player p3 = new Player(UUID.randomUUID(),"TestPlayer3");

        game.getPlayerSystem().addPlayer(p2);
        game.getPlayerSystem().addPlayer(p3);

        assertTrue(game.getPlayerSystem().getPlayers().stream().allMatch(player->
            game.getHandSystem().getPlayerHands().keySet().contains(player.getPlayerUUID())
                && game.getRewardSystem().getPlayerBets().keySet().contains(player.getPlayerUUID())
                && game.getRewardSystem().getPlayerBanks().keySet().contains(player.getPlayerUUID())));
    }

}
