package blackjack.services;

import blackjack.TestUtils;
import blackjack.entities.Player;
import blackjack.game.Game;
import blackjack.utils.GameException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.UUID;

import static org.junit.Assert.*;

public class GameServiceTest {
    private UUID game1UUID = UUID.randomUUID();
    private UUID game2UUID = UUID.randomUUID();

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void addingGameTest1(){

        GameService.get().createNewGame(game1UUID,"testGame",1,
            new Player(UUID.randomUUID(),"testPlayer"));

        assertTrue(
            GameService.get().getGames().stream()
                .map(Game::getGameUUID)
                .anyMatch(g->g.equals(game1UUID)));
    }

    @Test
    public void addingGameTest2(){
        GameService.get().createNewGame(game1UUID,"testGame",1,
            new Player(UUID.randomUUID(),"testPlayer"));
        GameService.get().createNewGame(game2UUID,"testGame",1,
            new Player(UUID.randomUUID(),"testPlayer"));

        assertTrue(
            GameService.get().getGames().stream()
                .map(Game::getGameUUID)
                .anyMatch(g->g.equals(game1UUID)));

        assertTrue(
            GameService.get().getGames().stream()
                .map(Game::getGameUUID)
                .anyMatch(g->g.equals(game2UUID)));
    }

    @Test
    public void removingGameTest1(){
        GameService.get().createNewGame(game1UUID,"testGame",1,
            new Player(UUID.randomUUID(),"testPlayer"));
        GameService.get().removeGame(game1UUID);

        assertFalse(
            GameService.get().getGames().stream()
                .map(Game::getGameUUID)
                .anyMatch(g->g.equals(game1UUID)));
    }

    @Test
    public void removingGameTest2(){
        GameService.get().createNewGame(game1UUID,"testGame",1,
            new Player(UUID.randomUUID(),"testPlayer"));
        GameService.get().createNewGame(game2UUID,"testGame",1,
            new Player(UUID.randomUUID(),"testPlayer"));
        GameService.get().removeGame(game1UUID);
        GameService.get().removeGame(game2UUID);

        assertFalse(
            GameService.get().getGames().stream()
                .map(Game::getGameUUID)
                .anyMatch(g->g.equals(game1UUID)));

        assertFalse(
            GameService.get().getGames().stream()
                .map(Game::getGameUUID)
                .anyMatch(g->g.equals(game2UUID)));
    }

    @Test
    public void gettingNonExistentGameTest(){
        exceptionRule.expect(GameException.class);
        exceptionRule.expectMessage("Game not found!");

        GameService.get().getGame(game1UUID);
    }

    @Test
    public void removingNonExistentGameTest(){

        exceptionRule.expect(GameException.class);
        exceptionRule.expectMessage("Game not found!");

        GameService.get().removeGame(game1UUID);
    }


    @Test
    public void dynamicMaxPlayerSize(){
        Player p1 = new Player(UUID.randomUUID(),"testPlayer");
        Player p2 = new Player(UUID.randomUUID(),"testPlayer");
        GameService.get().createNewGame(game1UUID,"testGame",2,p1);
        GameService.get().getGame(game1UUID).getPlayerSystem().addPlayer(p2);

        GameService.get().getGame(game1UUID).getRewardSystem().bet(p1.getPlayerUUID(),500);
        GameService.get().getGame(game1UUID).getRewardSystem().bet(p2.getPlayerUUID(),1500);

        TestUtils.finishGame(GameService.get().getGame(game1UUID));

        GameService.get().getGame(game1UUID).getPlayerSystem().removePlayer(p2.getPlayerUUID());

        GameService.get().getGame(game1UUID).getGameSystem().resetGame();

        assertEquals(1,GameService.get().getGame(game1UUID).getPlayerSystem().getMaxPlayers());
    }
}
