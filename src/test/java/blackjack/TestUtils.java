package blackjack;

import blackjack.game.Game;

import java.util.UUID;

public class TestUtils {
    public static Game getOFFgame() {
        return new Game(UUID.randomUUID(),3,"testRoom");
    }

    /**
     * finishes game by making every player,
     * that isn't already over or on 21, stand
     *
     * @param game game that is to be finished (has to be STARTED already!)
     */
    public static void finishGame(Game game){
        game.getPlayerSystem().getPlayers().forEach(player->{
            if(!game.getHandSystem().isOverOrOn21(player.getPlayerUUID())){
                game.getHandSystem().playerStand(player.getPlayerUUID());
            }
        });
    }

}
