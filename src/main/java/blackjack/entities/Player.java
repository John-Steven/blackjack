package blackjack.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

/**
 * Basic entity class representing
 * a player by UUID and name
 */
@Data
@AllArgsConstructor
public class Player {
    private UUID playerUUID;
    private String name;
}
