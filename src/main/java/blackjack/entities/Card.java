package blackjack.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Basic entity class representing
 * a playing card
 *
 * NUMBER:
 * 1  ->  1
 * 2  ->  2
 * 3  ->  3
 * 4  ->  4
 * 5  ->  5
 * 6  ->  6
 * 7  ->  7
 * 8  ->  8
 * 9  ->  9
 * 10 ->  10
 * 11 ->  PRINCE
 * 12 ->  QUEEN
 * 13 ->  KING
 *
 * TYPE:
 * 1  ->  HEARTS
 * 2  ->  TILES
 * 3  ->  CLOVERS
 * 4  ->  PIKES
 *
 */
@Data
@AllArgsConstructor
public class Card {
    private int number;
    private int type;
}
