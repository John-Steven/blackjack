package blackjack.controller;

import blackjack.entities.Player;
import blackjack.game.Game;
import blackjack.services.GameService;
import blackjack.services.SocketService;
import blackjack.utils.JSONUtil;
import blackjack.utils.SessionUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.javalin.websocket.WsSession;

import java.util.*;

/**
 * Controller class for processing
 * all player-related socket-messages
 */
public class PlayerController {


    /**
     * creates and joins a new player to a game,
     * connects the session to that game and
     * Updates clients with relevant data
     *
     * @param session session that wants to join the game
     * @param content {game_uuid,username}
     */
    public static void joinGame(WsSession session, JsonNode content){

        // game-logic-interaction
        Game game = GameService.get().getGame(UUID.fromString(content.get("game_uuid").asText()));
        Player player = new Player(UUID.randomUUID(),content.get("username").asText());
        game.getPlayerSystem().addPlayer(player);
        SocketService.get().connectSessionToGame(session, game.getGameUUID());

        //Update
        GameController.gameAndPlayerData(session,game.getGameUUID(),player.getPlayerUUID());
        HandController.updateHandInfo(content);
        PlayerController.updatePlayerInfo(content);
        GameController.updateAllGames();
        GameController.updateIfIsReady(content);
    }

    /**
     * removes a player from a game and
     * removes the sessions connection
     * to the game
     * also removes the game from the list
     * of games, if it has 0 players
     *
     * @param session that wants to leave the game
     * @param content {game_uuid, player_uuid}
     */
    public static void leaveGame(WsSession session, JsonNode content){

        // game-logic-interaction
        UUID gameUUID = UUID.fromString(content.get("game_uuid").asText());
        Game game = GameService.get().getGame(gameUUID);
        game.getPlayerSystem().removePlayer(UUID.fromString(content.get("player_uuid").asText()));

        if(session!=null) { // is null sometimes because of faked session call from forced close
            SocketService.get().removeSessionFromGame(session,gameUUID);
        }

        // Update
        if(game.getPlayerSystem().getPlayers().size()==0){
            GameService.get().removeGame(gameUUID);
        } else {
            HandController.updateHandInfo(content);
            GameController.updateIfIsFinished(content);
            PlayerController.updatePlayerInfo(content);
        }
        GameController.updateAllGames();
    }

    /**
     *  * updates all clients in game with
     *  * the uuid of the currPlayer and
     *  * all player names, uuids and bets
     *
     * @param content {game_uuid}
     */
    public static void updatePlayerInfo(JsonNode content){
        UUID gameUUID = UUID.fromString(content.get("game_uuid").asText());
        Game game = GameService.get().getGame(gameUUID);

        List<ObjectNode> nodes = new ArrayList<>();

        nodes.add(JSONUtil.newNode().put("curr_player_uuid",
            game.getPlayerSystem().getCurrPlayer().getPlayerUUID().toString()));

        game.getPlayerSystem().getPlayers().forEach(p->{
            ObjectNode n = JSONUtil.newNode();
            n.put("name",p.getName());
            n.put("player_uuid",p.getPlayerUUID().toString());
            n.put("bet_value", game.getRewardSystem().
                getPlayerBets().get(p.getPlayerUUID()));
            nodes.add(n);
        });

        SessionUtil.sendToAllInGame(gameUUID,JSONUtil.toJsonArray("playerInfo",nodes));
    }
}
