package blackjack.controller;

import blackjack.services.SocketService;
import blackjack.utils.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import io.javalin.websocket.WsSession;

/**
 * Controller class for session adding and removing
 * and all action routing
 */
public class SocketController {


    /**
     * Adds a newly connected session the list of active connections
     * and updates all connections with the newest list of games
     * (only updating this session would be sufficient)
     *
     * @param session session to add
     */
    public static void addSession(WsSession session){
        SocketService.get().getConnections().add(session);
        System.out.println("Connected new WS");
        GameController.updateAllGames();
    }

    /**
     * Central action routing
     *
     * Routes client messages
     * to other controllers
     *
     * @param session session who sent the message
     * @param message message that was send
     */
    public static void processMessage(WsSession session, String message){
        JsonNode jsonNode = JSONUtil.parseJson(message);
        String type = jsonNode.get("type").asText();
        JsonNode content = jsonNode.get("content");

        switch(type){

            case "draw":
                HandController.addCard(content);
                break;
            case "newGame" :
                GameController.createGame(session,content);
                break;
            case "stand" :
                HandController.stand(content);
                break;
            case "bet" :
                RewardController.bet(session,content);
                break;
            case "joinGame" :
                PlayerController.joinGame(session,content);
                break;
            case "playerBank" :
                RewardController.updatePlayerBank(session,content);
                break;
            case "leaveGame" :
                PlayerController.leaveGame(session,content);
                break;
            case "resetGame" :
                GameController.resetGame(content);
                break;
        }
    }

    /**
     * removes session from list of active connections
     * after it closed
     *
     * @param session session that closed
     * @param statusCode statusCode of closing
     * @param reason reason for the closing
     */
    public static void closeSession(WsSession session, int statusCode, String reason){
        SocketService.get().getConnections().remove(session);
        System.out.println(statusCode + " : Closed " + session + " because: " + reason);
    }
}
