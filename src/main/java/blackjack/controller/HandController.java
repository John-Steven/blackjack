package blackjack.controller;

import blackjack.entities.Card;
import blackjack.game.Game;
import blackjack.services.GameService;
import blackjack.utils.JSONUtil;
import blackjack.utils.SessionUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Controller class for processing
 * all hand-related socket-messages
 */
public class HandController {

    /**
     * draws a card for a player
     * and updates clients with relevant data
     *
     * @param content {game_uuid,player_uuid}
     */
    public static void addCard(JsonNode content){

        // game-logic-interaction
        Game game = GameService.get().getGame(UUID.fromString(content.get("game_uuid").asText()));
        game.getHandSystem().playerDraw(UUID.fromString(content.get("player_uuid").asText()));

        // Update
        HandController.updateHandInfo(content);
        PlayerController.updatePlayerInfo(content);
        GameController.updateIfIsFinished(content);

    }

    /**
     * makes a player pass his turn
     * and updates clients with relevant data
     *
     * @param content
     */
    public static void stand(JsonNode content){

        // game-logic-interaction
        GameService.get().getGame(UUID.fromString(content.get("game_uuid").asText()))
            .getHandSystem().playerStand(UUID.fromString(content.get("player_uuid").asText()));

        // Update
        HandController.updateHandInfo(content);
        PlayerController.updatePlayerInfo(content);
        GameController.updateIfIsFinished(content);
    }

    /**
     * updates all clients in game with
     * all playerhands/values,
     * as well as dealerhand/value
     *
     * @param content {game_uuid}
     */
    public static void updateHandInfo(JsonNode content){

        UUID gameUUID = UUID.fromString(content.get("game_uuid").asText());
        Game game = GameService.get().getGame(gameUUID);

        List<ObjectNode> nodes = new ArrayList<>();

        ObjectNode n1 = JSONUtil.newNode();
        n1.put("dealer_value", game.getHandSystem().getDealerValue());
        n1.put("dealer_hand", renderHand(game.getHandSystem().getDealerHand()));
        nodes.add(n1);

        game.getPlayerSystem().getPlayers().forEach(player->{
            ObjectNode n2 = JSONUtil.newNode();

            n2.put("value", game.getHandSystem().getHandValue(
                game.getHandSystem().getPlayerHand(player.getPlayerUUID())));

            n2.put("player_hand",renderHand(game.getHandSystem().getPlayerHand(
                player.getPlayerUUID())));

            nodes.add(n2);
        });

        SessionUtil.sendToAllInGame(gameUUID,JSONUtil.toJsonArray("handInfo",nodes));

    }

    /**
     * renders HTML for a hand
     *
     * a card is mapped to an .png
     * by its type and number in imgName
     *
     * rotateDegree and startDegree make it
     * so the cards of a hand are always evenly
     * separated by 30°
     *
     * @param hand hand to render as HTML
     * @return rendered HTML hand
     */
    public static String renderHand(List<Card> hand){
        String handHTML = "";
        int rotateDegree = 30;
        int startDegree = (-rotateDegree/2)*(hand.size()-1);

        for(Card card:hand){

            String imgName = "" + card.getType() + card.getNumber();

            String styleChange = "style=\"transform-origin:bottom; transform:translate(-50%, 0)"
                +"rotateZ(" + startDegree + "deg)\"";

            handHTML = handHTML + "<img id=picTest class=cardBox " + styleChange + " src=img/"
                + imgName + ".png></img>";

            startDegree = startDegree + rotateDegree;

        }

        return handHTML;
    }
}
