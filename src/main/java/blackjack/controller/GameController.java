package blackjack.controller;

import blackjack.entities.Player;
import blackjack.game.Game;
import blackjack.services.GameService;
import blackjack.services.SocketService;
import blackjack.utils.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.javalin.websocket.WsSession;

import java.util.*;

/**
 * Controller class for processing
 * all game-related socket-messages
 */
public class GameController {

    /**
     * creates a new game with a player,
     * connects the session to the game
     * and updates clients with relevant data
     *
     * @param session to connect to game
     * @param content {game_name, max_players, username}
     */
    public static void createGame(WsSession session, JsonNode content) {

        // game-logic-interaction
        UUID gameUUID = UUID.randomUUID();
        Player player = new Player(UUID.randomUUID(), content.get("username").asText());
        GameService.get().createNewGame(gameUUID, content.get("game_name").asText(),
            content.get("max_players").asInt(), player);
        SocketService.get().connectSessionToGame(session,gameUUID);



        // content doesn't contain game_uuid so I fake a client-request, so I can update
        JsonNode fakeContent = JSONUtil.newNode().put("game_uuid",gameUUID.toString());
        // Update
        GameController.gameAndPlayerData(session,gameUUID,player.getPlayerUUID());
        GameController.updateAllGames();
        GameController.updateIfIsReady(fakeContent);
        HandController.updateHandInfo(fakeContent);
        PlayerController.updatePlayerInfo(fakeContent);
    }

    /**
     * starts a new round in a game
     * by resetting it and updates
     * clients with relevant data
     *
     * @param content {game_uuid}
     */
    public static void resetGame(JsonNode content){

        // game-logic-interaction
        UUID gameUUID = UUID.fromString(content.get("game_uuid").asText());
        GameService.get().getGame(gameUUID).getGameSystem().resetGame();


        // Update
        SessionUtil.sendToAllInGame(gameUUID,
            JSONUtil.toJson("resetGame",JSONUtil.newNode()));
        updateAllGames();
        HandController.updateHandInfo(content);
        GameController.updateIfIsReady(content);
        PlayerController.updatePlayerInfo(content);
    }

    /**
     * sends the rendered list of games
     * to all clients
     */
    public static void updateAllGames(){
        SessionUtil.sendToAll(JSONUtil.toJson("allGames",
            JSONUtil.newNode().put("games_list",renderAllGames())));
    }


    /**
     * checks if a game is ready,
     * if it is, it sends a ping "isReady"
     * to all session connected to the game
     *
     * @param content {game_uuid}
     */
    public static void updateIfIsReady(JsonNode content){
        Game game = GameService.get().getGame(UUID.fromString(content.get("game_uuid").asText()));

        if(game.getPlayerSystem().isReady()){
            SessionUtil.sendToAllInGame(game.getGameUUID(),JSONUtil.toJson("isReady",
                JSONUtil.newNode()));
        }
    }

    /**
     * checks if a game is finished
     * if it is, it sends a ping "isFinished"
     * to all sessions connected to the game
     *
     * @param content {game_uuid}
     */
    public static void updateIfIsFinished(JsonNode content){
        UUID gameUUID = UUID.fromString(content.get("game_uuid").asText());

        if(GameService.get().getGame(gameUUID).getGameSystem().isFinished()){
            SessionUtil.sendToAllInGame(gameUUID,JSONUtil.toJson("isFinished",
                JSONUtil.newNode()));
        }
    }

    /**
     * renders HTML for all games
     * as elements for a list
     *
     * if a game is full, it's HTML
     * becomes unclickable
     *
     * @return HTML of a list of all games
     */
    public static String renderAllGames(){
        StringBuilder listHTML = new StringBuilder();

        // if there are no games: Send an "empty" element
        if(GameService.get().getGames().size()==0){
            listHTML.append("<button type=\"button\" class=\"gameButton list-group-item "
                    + "list-group-item-action text-center\" style=\"background-color:transparent\""
                    + " disabled> Empty</button>"
                );
        }
        else {
            GameService.get().getGames().forEach(game ->
                listHTML.append(
                    ("<button type=\"button\" data-uuid=\"" + game.getGameUUID()
                        + "\" class=\"gameButton list-group-item list-group-item-action text-center\" "
                        + "style=\"background-color: transparent;\""
                        + (game.getPlayerSystem().isReady() ? "disabled" : "") + ">"
                        + game.getName() + " " + game.getPlayerSystem().getPlayers().size()
                        + "/" + game.getPlayerSystem().getMaxPlayers() + "</button>")
                ));
        }

        return listHTML.toString();
    }

    /**
     * sends game_uuid and player_uuid to a session,
     * which just created or joined a game
     *
     * @param session session from which the games was created/joined
     * @param gameUUID uuid of the game created/joined
     * @param playerUUID uuid of the player that was created by creating/joining
     */
    public static void gameAndPlayerData(WsSession session, UUID gameUUID, UUID playerUUID){
        ObjectNode node = JSONUtil.newNode();
        node.put("game_uuid", gameUUID.toString());
        node.put("player_uuid",playerUUID.toString());
        session.getRemote().sendStringByFuture(JSONUtil.toJson("newGame",node));
    }
}
