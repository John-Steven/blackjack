package blackjack.controller;

import blackjack.game.Game;
import blackjack.services.GameService;
import blackjack.utils.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.javalin.websocket.WsSession;
import java.util.UUID;

/**
 * Controller class for processing
 * all reward-related socket-messages
 */
public class RewardController {

    /**
     * places a player's bet
     * and updates clients with relevant data
     *
     * @param session whose player bet
     * @param content {game_uuid,player_uuid,cash}
     */
    public static void bet(WsSession session, JsonNode content){
        Game game = GameService.get().getGame(UUID.fromString(content.get("game_uuid").asText()));

        game.getRewardSystem().bet(UUID.fromString(content.get("player_uuid").asText()),
            content.get("cash").asInt());

        // Update
        PlayerController.updatePlayerInfo(content);
        RewardController.updatePlayerBank(session,content);
        if(game.getRewardSystem().allHaveBet()){
            HandController.updateHandInfo(content);
            GameController.updateIfIsFinished(content);
        }
    }

    /**
     * sends the player's bank to the session
     * and also if the game is finished
     * (relevant for blinking text on client side)
     *
     * the playerBank can not be sent via updatePlayerInfo,
     * because the playerBank should only be sent to the session,
     * that actually belongs to that player. Because the server
     * does not store a player/session map, this data has to
     * be explicitly requested from the client, so the server
     * knows which session to send it to. (see playerBank() in main.js)
     *
     * @param session session to send the player's bank to
     * @param content {game_uuid,player_uuid}
     */
    public static void updatePlayerBank(WsSession session, JsonNode content){
        UUID gameUUID = UUID.fromString(content.get("game_uuid").asText());
        UUID playerUUID = UUID.fromString(content.get("player_uuid").asText());

        Game game = GameService.get().getGame(gameUUID);

        ObjectNode n = JSONUtil.newNode();
        n.put("player_bank", game.getRewardSystem().getPlayerBanks().get(playerUUID));
        n.put("is_finished",GameService.get().getGame(gameUUID).getGameSystem().isFinished());

        session.getRemote().sendStringByFuture(JSONUtil.toJson("playerBank",n));
    }
}
