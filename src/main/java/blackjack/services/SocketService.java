package blackjack.services;
import blackjack.utils.GameException;
import io.javalin.websocket.WsSession;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Singleton class for managing
 * all active sessions and all active
 * sessions, that are currently in a game
 */
@Getter
public class SocketService {

    private static SocketService instance;

    private Map<WsSession, UUID> gameConnections;
    private List<WsSession> connections;

    private SocketService(){
        gameConnections = new HashMap<>();
        connections = new ArrayList<>();
    }

    public static SocketService get(){
        return instance = Optional.ofNullable(instance).orElse(new SocketService());
    }

    /**
     * connects a session to game
     *
     * @param session session to connect
     * @param gameUUID uuid of game to connect to
     */
    public void connectSessionToGame(WsSession session, UUID gameUUID){
        gameConnections.put(session,gameUUID);
    }

    /**
     * gets all sessions that are connected to a certain game
     *
     * @param gameUUID uuid of the game whose connected sessions to get
     * @return all sessions connected to the game
     */
    public List<WsSession> getAllSessionsInGame(UUID gameUUID){
        return gameConnections.entrySet().stream().filter(e->e.getValue().equals(gameUUID))
            .map(Map.Entry::getKey).collect(Collectors.toList());
    }

    /**
     * removes a game/session-entry
     *
     * @throws GameException if the entry does not exist
     *
     * @param session session of pair to remove
     * @param gameUUID uuid of pair to remove
     */
    public void removeSessionFromGame(WsSession session, UUID gameUUID){
        if(!gameConnections.entrySet().contains(Map.entry(session,gameUUID))){
            throw new GameException("No session/game entry");
        }
        gameConnections.entrySet().remove(Map.entry(session,gameUUID));
    }
}
