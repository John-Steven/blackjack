package blackjack.services;

import blackjack.entities.Player;
import blackjack.game.Game;
import blackjack.utils.GameException;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Singleton class for managing all active games
 */
@Getter
public class GameService {

    private static GameService instance;
    private List<Game> games;

    private GameService(){
        games = new ArrayList<>();
    }

    public static GameService get() {
        return instance = Optional.ofNullable(instance).orElse(new GameService());
    }


    /**
     * returns an active game
     *
     * @throws GameException if game does not exist
     *
     * @param gameUUID uuid of the game to get
     * @return the game
     */
    public Game getGame(UUID gameUUID){
        return games.stream()
            .filter(game-> game.getGameUUID().equals(gameUUID))
            .findFirst().orElseThrow(()->new GameException("Game not found!"));
    }

    /**
     * creates a new game, adds the firstPlayer
     * and adds it to the list of games
     *
     * @param gameUUID of the game to create
     * @param name name of the game to create
     * @param maxPlayers maximum amount of player of the game to create
     * @param firstPlayer the firstPlayer to be added to the game
     */
    public void createNewGame(UUID gameUUID,String name, int maxPlayers, Player firstPlayer){
        games.add(new Game(gameUUID,maxPlayers,name));
        getGame(gameUUID).getPlayerSystem().addPlayer(firstPlayer);
    }


    /**
     * removes a game from the list of active games
     *
     * @param gameUUID uuid of the game to remove
     */
    public void removeGame(UUID gameUUID){
        games.remove(getGame(gameUUID));
    }

}
