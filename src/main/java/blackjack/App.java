package blackjack;

import blackjack.controller.*;
import io.javalin.Javalin;
public class App {

    public static void main(String[] args){

        Javalin.create()
            .ws("/websockets", ws -> {
                    ws.onConnect(SocketController::addSession);
                    ws.onClose(SocketController::closeSession);
                    ws.onMessage(SocketController::processMessage);
            })
            .enableStaticFiles("/public")
            .start(8080);
    }
}