package blackjack.game;

import blackjack.entities.Card;

import java.util.List;
import java.util.UUID;

interface HandManageable{
        /**
         * calculates the value of a hand
         * after the rules of blackjack
         *
         * @param cards hand of cards whose value is to be calculated
         * @return value of the hand
         */
        int getHandValue(List<Card> cards);

        /**
         * draws a card for a player
         *
         * @param playerUUID uuid of the player who should draw
         */
        void playerDraw(UUID playerUUID);

        /**
         * passes the turn to the nextPlayer
         *
         * @param playerUUID of the player who should pass his turn
         */
        void playerStand(UUID playerUUID);

        /**
         * draws a card for the dealer
         */
        void dealerDraw();
    }
