package blackjack.game;

import blackjack.entities.Player;
import blackjack.utils.GameException;
import lombok.Getter;

import java.util.*;

import static blackjack.utils.GameState.*;

/**
 * Class representing all the players
 * in the game and allowing for interaction
 * with them
 * (important) Also responsible for keeping
 * the playerHands, the playerBets and the playerBanks
 * synchronized with the actually players in the Game
 */

@Getter
public class PlayerSystem implements PlayerManageable{
    private List<Player> players;
    private Player currPlayer;
    private int maxPlayers;

    private final Game game;

    private static Player dummy = new Player(UUID.randomUUID(),"Dummy");


    /**
     * @throws GameException when the maxPlayer is set too high or low
     */
    public PlayerSystem(Game game, int maxPlayers){
        if(maxPlayers>3 || maxPlayers<0){
            throw new GameException("Illegal maxPlayers value!");
        }
        this.game = game;
        this.maxPlayers = maxPlayers;
        players = new ArrayList<>();
    }

    /**
     * resets the playerSystem for a
     * already existing game (not a new one!)
     */
    public void init(){
        currPlayer = players.get(0);
        maxPlayers = players.size();
        game.getGameSystem().changeGameState(BET);
    }

    /**
     * gets a player by his uuid
     *
     * @throws GameException if the player is not in the game
     *
     * @param playerUUID uuid of player to get
     * @return the player
     */
    public Player getPlayer(UUID playerUUID){
        return players.stream()
            .filter(player->player.getPlayerUUID().equals(playerUUID))
            .findFirst().orElseThrow(()->new GameException("Player does not exist!"));
    }

    /**
     * gets the player whose turn it is
     *
     * since it is no one's turn when the
     * gameState is not PLAYERS_CHOOSING return a dummy
     *
     * @return the current player
     */
    public Player getCurrPlayer(){
        if(!game.getGameSystem().getGameState().equals(PLAYERS_CHOOSING)){
            return dummy;
        } else {
            return currPlayer;
        }
    }

    /**
     * adds a player to the game and enter him
     * in the playerHands, playerBets and playerBank
     *
     * sets the player as currPlayer if he is the first
     * and starts the game if it is full
     *
     * @throws GameException when the game is full or the player already exists
     *
     * @param player to add to the game
     */
    public void addPlayer(Player player){
        if(players.contains(player) || players.size()>=maxPlayers){
            throw new GameException("Player can not be added!");
        } else {
            players.add(player);
            if(players.size()==1) {
                currPlayer = player;
            }
            game.getHandSystem().getPlayerHands().put(player.getPlayerUUID(),new ArrayList<>());
            game.getRewardSystem().getPlayerBanks().put(player.getPlayerUUID(),5000);
            game.getRewardSystem().bet(player.getPlayerUUID(),0);

            if(isReady()){
                game.getGameSystem().changeGameState(BET);
            }
        }
    }

    /**
     * removes a player from the game and removes his
     * entries in the playerHands, playerBets and playerBanks
     *
     * if the player was the currPlayer go to nextPlayer
     * and if the game is empty now set it to FINISHED
     *
     *
     * @param playerUUID uuid of player to remove
     */
    public void removePlayer(UUID playerUUID){
        Player player = getPlayer(playerUUID);

        if(game.getPlayerSystem().getCurrPlayer().getPlayerUUID().equals(player.getPlayerUUID())
            && game.getGameSystem().getGameState().equals(PLAYERS_CHOOSING)){
            nextPlayer();
        }
        players.remove(player);
        game.getHandSystem().getPlayerHands().remove(player.getPlayerUUID());
        game.getRewardSystem().getPlayerBanks().remove(player.getPlayerUUID());
        game.getRewardSystem().getPlayerBets().remove(player.getPlayerUUID());

        if (players.size()==0){
            game.getGameSystem().changeGameState(FINISHED);
        }
    }

    /**
     * passes the turn to the next player
     * and if it was the last player set the
     * game to DEALER_DRAW
     */
    public void nextPlayer(){
        if(players.indexOf(currPlayer)+1==players.size()){
            game.getGameSystem().changeGameState(DEALER_DRAW);
        } else {
            currPlayer = players.get(((players.indexOf(currPlayer) + 1)));
        }
    }

    /**
     * checks if the game has
     * reached maxPlayers
     *
     * @return is the game ready
     */
    public boolean isReady(){
        return players.size()>= maxPlayers;
    }
}
