package blackjack.game;

import lombok.Getter;

import java.util.UUID;

/**
 * Class representing a game
 * with a name, UUID and system-classes
 * for the game-logic
 */
@Getter
public class Game {
    private final String name;
    private final UUID gameUUID;
    private final GameSystem gameSystem;
    private final CardSystem cardSystem;
    private final PlayerSystem playerSystem;
    private final HandSystem handSystem;
    private final RewardSystem rewardSystem;

    public Game(UUID gameUUID,int maxPlayers, String name){
        this.gameUUID = gameUUID;
        this.name = name;
        cardSystem = new CardSystem(this);
        gameSystem = new GameSystem(this);
        playerSystem = new PlayerSystem(this,maxPlayers);
        handSystem = new HandSystem(this);
        rewardSystem = new RewardSystem(this);
    }

}
