package blackjack.game;

import blackjack.entities.Player;
import blackjack.utils.GameException;
import lombok.Getter;

import java.util.*;

import static blackjack.utils.GameState.*;

@Getter
public class RewardSystem implements RewardManageable {

    private final Game game;

    private Map<UUID,Integer> playerBets;
    private Map<UUID,Integer> playerBanks;

    public RewardSystem(Game game){
        this.game = game;
        playerBanks = new HashMap<>();

        init();
    }

    /**
     * initializes the playerBets as empty map
     * or, if it's not the first round, sets all
     * playerBets back to 0
     *
     */
    public void init(){
        Map<UUID,Integer> newPlayerInputCash = new HashMap<>();
        if(playerBets !=null){
            playerBets.keySet().forEach(playerUUID-> newPlayerInputCash.put(playerUUID,0));
        }
        playerBets = newPlayerInputCash;

    }

    /**
     * puts a player's bet into playerBets
     * and removes that amount from the player's bank
     *
     * if the game bet is higher than the player's bank
     * it's floored to 0
     * and if all player have bet, starts the game
     *
     * @throws GameException when the player has already bet
     *
     * @param playerUUID uuid of player who wants to bet
     * @param cash amount of cash he wants to bet
     */
    public void bet(UUID playerUUID, int cash){
        if(!game.getGameSystem().getGameState().equals(OFF) && playerBets.get(playerUUID)!=0){
            throw new GameException("Player has already bet!");
        }
        cash = cash>playerBanks.get(playerUUID)?0:cash;
        playerBets.put(playerUUID,cash);
        playerBanks.put(playerUUID, playerBanks.get(playerUUID)-cash);
        if(allHaveBet()){
            game.getGameSystem().changeGameState(STARTED);
        }
    }

    /**
     * processes the rewards for all players
     *
     * (Rule: only the players who are not over 21
     *  don't get cash back)
     *
     *  (Rule: if the player's value is equal to the dealer's,
     *  he gets his bet back)
     *
     *  (Rule: if the player's value is higher than the dealer's
     *  or the dealer overdrew the player gets his bet x 2)
     *
     */
    public void processRewards(){
        game.getPlayerSystem().getPlayers().stream().map(Player::getPlayerUUID).forEach(playerUUID->{
            if(!game.getHandSystem().isOver21(playerUUID)){
                if(equalToDealer(playerUUID)){
                    giveBackBet(playerUUID);
                } else if (higherThanDealer(playerUUID) || game.getHandSystem().getDealerValue()>21){
                    giveBackDoubleBet(playerUUID);
                }
            }
        });
    }

    /**
     * checks if the player's value is higher than the dealer's
     *
     * @param playerUUID uuid of the player whom to compare
     * @return if the player's value is higher than the dealer's
     */
    public boolean higherThanDealer(UUID playerUUID){
        return game.getHandSystem()
            .getHandValue(game.getHandSystem().getDealerHand()) < game.getHandSystem()
            .getHandValue(game.getHandSystem().getPlayerHands().get(playerUUID));
    }

    /**
     * checks if the player's value is equal to the dealer's
     *
     * @param playerUUID uuid of the player whom to compare
     * @return if the player's value is equal to the dealer's
     */
    public boolean equalToDealer(UUID playerUUID){
        return game.getHandSystem().getHandValue(game.getHandSystem()
            .getPlayerHands().get(playerUUID)) == game.getHandSystem().getDealerValue();
    }

    /**
     * adds the player's bet x 2 to his bank
     *
     * @param playerUUID uuid of player whom receives double his bet
     */
    public void giveBackDoubleBet(UUID playerUUID){
        playerBanks.put(playerUUID, playerBanks.get(playerUUID)
            + playerBets.get(playerUUID)*2);
    }

    /**
     * adds the player's bet to his bank
     *
     * @param playerUUID uuid of player whom receives his bet
     */
    public void giveBackBet(UUID playerUUID){
        playerBanks.put(playerUUID, playerBanks
            .get(playerUUID)+ playerBets.get(playerUUID));
    }

    /**
     * checks if all players have given a bet
     *
     * @return if all players have bet
     */
    public boolean allHaveBet(){
        return playerBets.values().stream().allMatch(i->i!=0);
    }
}
