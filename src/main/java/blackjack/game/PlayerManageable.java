package blackjack.game;

import blackjack.entities.Player;

import java.util.UUID;

interface PlayerManageable{
    /**
     * adds a player to the round of blackjack
     *
     * @param player player to add
     */
    void addPlayer(Player player);

    /**
     * removes a player from the game
     *
     * @param playerUUID uuid of the player who should be removed
     */
    void removePlayer(UUID playerUUID);

    /**
     * gets the player whose turn it is
     *
     * @return the player whose turn it is
     */
    Player getCurrPlayer();
}
