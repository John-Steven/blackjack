package blackjack.game;

import blackjack.utils.GameState;

interface GameManageable{
    /**
     * main function for controlling the
     * gameState and how to react to
     * state-changes
     *
     * @param gameState new gameState
     */
    void changeGameState(GameState gameState);
}
