package blackjack.game;

import blackjack.entities.Player;
import blackjack.utils.GameState;
import lombok.Getter;

import static blackjack.utils.GameState.*;

/**
 * Class for managing the gameState
 * and the basics of the game-procedures
 */
@Getter
public class GameSystem implements GameManageable {

    private GameState gameState;

    private final Game game;

    /**
     * main function for controlling the
     * gameState and how to react to
     * state-changes
     *
     * @param gameState new gameState
     */
    public void changeGameState(GameState gameState){

        if(this.gameState == gameState) return;
        this.gameState = gameState;

        switch(gameState){
            case STARTED:{
                game.getHandSystem().dealerDraw();
                game.getHandSystem().getPlayerHands().keySet().forEach(game.getHandSystem()::playerDraw);
                game.getHandSystem().getPlayerHands().keySet().forEach(game.getHandSystem()::playerDraw);
                changeGameState(PLAYERS_CHOOSING);
                break;
            } case PLAYERS_CHOOSING: {
                // WHEN all players got 21 on their second draw skip
                if(game.getPlayerSystem().getPlayers().stream()
                    .map(Player::getPlayerUUID).allMatch(game.getHandSystem()::isOverOrOn21)){
                    changeGameState(DEALER_DRAW);
                }
                break;
            } case DEALER_DRAW: {
                // when all players are over 21, the dealer doesn't need to draw
                if(!game.getPlayerSystem().getPlayers().stream()
                    .map(Player::getPlayerUUID).allMatch(game.getHandSystem()::isOver21)){
                    game.getHandSystem().dealerDraw();
                }
                game.getRewardSystem().processRewards();
                changeGameState(FINISHED);
                break;
            }
        }
    }

    public GameSystem(Game game){
        init();
        this.game = game;
    }

    /**
     * checks if a game isFinished
     *
     * @return if the game is finished
     */
    public boolean isFinished(){
        return gameState.equals(GameState.FINISHED);
    }

    /**
     * starts a new round of a game
     *
     */
    public void resetGame(){
        game.getCardSystem().init();
        game.getGameSystem().init();
        game.getHandSystem().init();
        game.getPlayerSystem().init();
        game.getRewardSystem().init();
    }

    public void init(){
        changeGameState(OFF);
    }

}
