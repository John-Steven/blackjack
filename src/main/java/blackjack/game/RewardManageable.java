package blackjack.game;

import java.util.UUID;


public interface RewardManageable{
    /**
     * processes a player's bet
     *
     * @param playerUUID uuid of the player who bet
     * @param cash the player's bet
     */
    void bet(UUID playerUUID, int cash);

    /**
     * processes the rewards for all players
     */
    void processRewards();
}

