package blackjack.game;

import blackjack.entities.Card;
import blackjack.utils.GameException;
import lombok.Getter;

import java.util.*;

import static blackjack.utils.GameState.DEALER_DRAW;

/**
 * Class representing the hands of
 * all players and the dealer and
 * allowing for interaction with them
 */

@Getter
public class HandSystem implements HandManageable{

    private final Game game;
    private Map<UUID, List<Card>> playerHands;
    private List<Card> dealerHand;

    public HandSystem(Game game){
        this.game = game;
        init();
    }


    /**
     * initializes all player-hands as empty
     */
    public void init(){
        Map<UUID,List<Card>> newMap = new HashMap<>();
        if(playerHands!=null){
            playerHands.keySet().forEach(playerUUID -> newMap.put(playerUUID,new ArrayList<>()));
        }
        playerHands = newMap;
        dealerHand = new ArrayList<>();
    }

    /**
     * calculates the value of a given hand
     * the list has to be sorted in reverse-order
     * for this, because the ace is counted as a 1
     * or a 11 depending on, if the value goes over 21
     *
     * @param hand to calculate value for
     * @return value of the hand
     */
    public int getHandValue(List<Card> hand){
        int sum = 0;

        List<Card> sortedHand = new ArrayList<>(hand);
        sortedHand.sort(Comparator.comparingInt(Card::getNumber).reversed());

        for(Card card:sortedHand){
            if(card.getNumber()>=2 && card.getNumber()<=9) sum += card.getNumber();
            else if(card.getNumber()>=10 && card.getNumber()<=13) sum += 10;
            else if(sum+11<=21){
                sum+=11;
            } else {
                sum+=1;
            }
        }
        return sum;
    }

    /**
     * gets the dealerHand value
     *
     * @return the value of the dealer's hand
     */
    public int getDealerValue(){
        return getHandValue(dealerHand);
    }

    /**
     * gets a player's hand
     *
     * @throws GameException when the player does not exist
     *
     * @param playerUUID whose hand to get
     * @return the player's hand
     */
    public List<Card> getPlayerHand(UUID playerUUID){
        if(!playerHands.containsKey(playerUUID)){
            throw new GameException("Player does not exist");
        }
        return playerHands.get(playerUUID);
    }

    /**
     * checks if player is over or on 21
     *
     * @param playerUUID player whose hand to check
     * @return if the player is over or on 21
     */
    public boolean isOverOrOn21(UUID playerUUID){
        return getHandValue(playerHands.get(playerUUID))>=21;
    }

    /**
     * checks if player is over 21
     *
     * @param playerUUID player whose hand to check
     * @return if the player is over 21
     */
    public boolean isOver21(UUID playerUUID) {
        return getHandValue(playerHands.get(playerUUID))>21;
    }


    /**
     * draws a card for a dealer
     * if the gameState is DEALER_DRAW
     * he draws until he has a
     * value over 16 ( Rule: dealer draws on 16 and stands on 17)
     */
    public void dealerDraw(){
        dealerHand.add(game.getCardSystem().drawCard());
        if(game.getGameSystem().getGameState().equals(DEALER_DRAW)){
            if(getHandValue(dealerHand)<=16){
                dealerDraw();
            }
        }
    }

    /**
     * draws a card for a player
     * and goes to nextPlayer, if he
     * overdraws and is the currPlayer
     *
     * @throws GameException when the player is already over or on 21
     *
     * @param playerUUID uuid of the player who wants to draw
     */
    public void playerDraw(UUID playerUUID){
        if(isOverOrOn21(playerUUID)){
            throw new GameException("Player can't draw!");
        } else {
            playerHands.get(playerUUID).add(game.getCardSystem().drawCard());
        }

        if(isOverOrOn21(playerUUID)
            && game.getPlayerSystem().getCurrPlayer().getPlayerUUID().equals(playerUUID)) {
            game.getPlayerSystem().nextPlayer();
        }
    }

    /**
     * the player stands and passes his turn
     * to the nextPlayer
     *
     * @throws GameException when the player isn't the currPlayer
     *
     * @param playerUUID
     */
    public void playerStand(UUID playerUUID){
        if(!playerUUID.equals(game.getPlayerSystem().getCurrPlayer().getPlayerUUID())){
            throw new GameException("Cannot stand, because notCurrPlayer!");
        } else {
            game.getPlayerSystem().nextPlayer();
        }
    }

}
