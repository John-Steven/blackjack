package blackjack.game;

import blackjack.entities.Card;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 *  Class representing a deck of cards
 *
 */
@Getter
public class CardSystem implements CardManageable {


    private final Game game;

    private List<Card> allCards;

    public CardSystem(Game game){
        this.game = game;
        init();
    }


    /**
     *  calculates a random card idx
     *  to draw and remove from the deck
     *
     * @return card drawn
     */
    public Card drawCard(){
        if(allCards.size()==0) init();

        // calc random card pos

        // return cardDrawn
        return allCards.remove(new Random().nextInt(allCards.size()));
    }

    /**
     * fills the deck
     * with 6 of each card
     */
    public void init(){
        allCards = new ArrayList<>(312);

        IntStream.range(0,6).forEach(n->IntStream.rangeClosed(1,4)
            .forEach(i-> IntStream.rangeClosed(1,13).forEach(j-> allCards.add(new Card(j,i)))));
    }



}
