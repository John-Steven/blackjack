package blackjack.game;

import blackjack.entities.Card;

interface CardManageable{
    /**
     * draws a card from a deck
     *
     * @return card drawn
     */
    Card drawCard();
}
