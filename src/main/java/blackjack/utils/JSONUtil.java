package blackjack.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.*;

/**
 * Utility class for mapping JsonObjects into a set
 * format and then parsing them to Strings,
 * and also parsing Strings to Json
 */
public class JSONUtil {

    private static ObjectMapper mapper = new ObjectMapper();

    public static ObjectNode newNode(){ return mapper.createObjectNode(); }

    /**
     * parses a String to Json if possible
     *
     * @param jsonString string to parse to json
     * @return parsed jsonNode (or null if failed)
     */
    public static JsonNode parseJson(String jsonString) {
        try {
            return new ObjectMapper().readTree(jsonString);
        } catch (Exception ex) {
            return null;
        }
    }


    /**
     * maps a Json into a set format
     * and parses it to String
     *
     *  * JSON-FORMAT:
     *  *
     *  * {
     *  *     type: <ACTION IDENTIFIER>
     *  *     content: <MSG DATA>
     *  * }
     *
     * @param type action identifier
     * @param content content of message
     * @return parsed json-string (or null if failed)
     */
    public static String toJson(String type, ObjectNode content){
        try {
            ObjectNode rootNode = mapper.createObjectNode();
            rootNode.put("type", type);
            rootNode.set("content", content);
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);

        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * maps a Json into a set format
     * and parses it to String
     *
     *  * JSON-FORMAT:
     *  *
     *  * {
     *  *     type: <ACTION IDENTIFIER>
     *  *     content: {<MSG_DATA1>,<MSG_DATA2>,...}
     *  *
     *  * }
     *
     * @param type action identifier
     * @param content content of message
     * @return parsed json-string (or null if failed)
     */
    public static String toJsonArray(String type, List<ObjectNode> content){

        try {
            ArrayNode arrayNode = mapper.createArrayNode();
            ObjectNode rootNode = mapper.createObjectNode();
            rootNode.put("type", type);
            content.forEach(arrayNode::add);
            rootNode.set("content", arrayNode);
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
        } catch (Exception e){
            return null;
        }
    }


}
