package blackjack.utils;

public enum GameState {OFF, BET, STARTED, PLAYERS_CHOOSING, DEALER_DRAW, FINISHED}