package blackjack.utils;

/**
 * Utility exception class for all game-related exceptions
 */
public class GameException extends IllegalArgumentException {
    public GameException (String msg){
        super(msg);
    }
}