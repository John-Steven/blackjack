package blackjack.utils;

import blackjack.services.SocketService;
import io.javalin.websocket.WsSession;

import java.util.UUID;

/**
 * Utility class for sending messages to session
 */
public class SessionUtil {
    /**
     * sends the message to all active connected sessions
     *
     * @param message message to send
     */
    public static void sendToAll(String message){
        SocketService.get().getConnections().stream().filter(WsSession::isOpen)
            .forEach(s-> s.getRemote().sendStringByFuture(message));
    }

    /**
     * sends the message to all active sessions connected to the game
     *
     * @param gameUUID uuid of the game whose sessions to send the message to
     * @param message message to send
     */
    public static void sendToAllInGame(UUID gameUUID, String message){
        SocketService.get().getAllSessionsInGame(gameUUID).stream().filter(WsSession::isOpen)
            .forEach(s-> s.getRemote().sendStringByFuture(message));
    }
}
