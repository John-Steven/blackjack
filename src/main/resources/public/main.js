var currGame;
var thisPlayer;
var player_idx;
var currBet = 0;

function onloadFc(){
        $(".ingameBTN").hide();
        $("#resetGameBTN").hide();
        $("#gameNotReadyLabel").hide();
        $(".chipBox").attr("disabled",true);
        $("#betBTN").attr("disabled",true);
        $(".chipBox").css("pointer-events","none");
        $("#createGameModal").modal('toggle');
}



$(function(){

    /**
     * leave game when the window is closed
     */
    $(window).on('beforeunload', function (){
        leaveGame();
    });
    $(window).on("unload", function (){
        leaveGame();
    });

    function sendWs(ofType,ofContent){
        ws.send(JSON.stringify({
            type : ofType,
            content : ofContent
        }));
    }

    let ws = new WebSocket("ws://" + location.hostname + ":" + location.port + "/websockets");



    /**
     * Central action routing
     *
     * Processes server messages
     */
    ws.onmessage = function(message){
        var json = JSON.parse(message.data);
        var type = json.type;
        var content = json.content;

        switch(type){
            case 'handInfo' :{
                renderHandInfos(content);
                break;

            } case 'playerInfo' :{
                renderPlayerInfos(content);
                break;

            } case 'newGame' : {
                currGame = content.game_uuid;
                thisPlayer = content.player_uuid;
                $("#gameNotReadyLabel").show();
                $("#dealerValue").hide();

                $("#hitBTN").attr("disabled", true);
                $("#standBTN").attr("disabled", true);
                break;

            } case 'isFinished' : {
                playerBank();
                if(currGame!=null){
                    $("#resetGameBTN").show();
                }
                break;

            } case 'allGames' : {
                $("#gamesList").html(content.games_list);
                break;

            } case 'resetGame' : {
                $("#resetGameBTN").hide();
                $(".chipBox").attr("disabled",false);
                $(".chipsBox").css("box-shadow","0px 8px 15px lightcoral");
                $("#bankOutput").css("animation","");
                break;
            } case 'playerBank' : {
                if(content.is_finished==true){
                    var blinkColor;
                    if(playerBank!=null){
                        if((parseInt(thisPlayerBank)+parseInt(currBet))>parseInt(content.player_bank)){
                            blinkColor = "Red";
                        } else if ((parseInt(thisPlayerBank)+parseInt(currBet))<parseInt(content.player_bank)){
                            blinkColor = "Green";
                        } else if((parseInt(thisPlayerBank)+parseInt(currBet))==parseInt(content.player_bank)){
                            blinkColor = "Grey";
                        }
                        $("#bankOutput").css("animation","blinker"+blinkColor+ " 0.75s step-start 3");
                        if(content.player_bank=="0"){leaveGame();}
                    }
                }

                thisPlayerBank = content.player_bank;
                $("#bankOutput").text(thisPlayerBank+" $");
                break;

            } case 'isReady' : {
                playerBank();

                $("#dealerValue").show();
                $("#gameNotReadyLabel").hide();
                $(".chipBox").attr("disabled",false);
                $(".chipsBox").css("box-shadow","0px 0px 15px lightcoral");
                $(".chipBox").css("pointer-events","auto");
                break;
            }
        }

    }

    ws.onclose = () => alert("WebSocket connection closed");





    /**
     *  button-click processing
     * 
     */
    
    $(document).on("click","#createGameBTN",function(){
        if($("#gameNameInput").val()!="" && $("#adminNameInput").val()!=""){
        sendWs('newGame',{game_name : $("#gameNameInput").val(),max_players : $("input:radio[name ='maxPlayersRadio']:checked").val(),
            username : $("#adminNameInput").val()});

        $("#createGameModal").modal('toggle');
        $(".ingameBTN").show();
        }
    });

    $(document).on("click","#hitBTN",function(){
        sendWs('draw',{game_uuid: currGame, player_uuid : thisPlayer});
    });

    $(document).on("click","#standBTN",function(){
        sendWs('stand',{game_uuid: currGame, player_uuid : thisPlayer});
    })

    $(document).on("click","#betBTN",function(){
        sendWs('bet',{game_uuid: currGame, player_uuid : thisPlayer,cash : currBet});

        $("#betBTN").attr("disabled",true);
        $(".chipBox").attr("disabled",true);
        $(".chipsBox").css("box-shadow","");
        $(".chipBox").css("pointer-events","none");
    })

    $(document).on("click","#resetGameBTN",function(){
        sendWs('resetGame',{game_uuid: currGame});
        currBet = 0;
        $(".chipBox").css("pointer-events","auto");
    })

    $(document).on("click",".gameButton",function(){
        var game_uuid = $(this).data('uuid');
        $("#joinGameBTN").data("game_uuid",game_uuid);
    });

    $(document).on("click",".chipBox",function(){
        var amount = $(this).data('amount');
        if(amount==-1){
            amount = parseInt(thisPlayerBank);
        }
        currBet = currBet + amount;

        if(currBet>thisPlayerBank){
            currBet=thisPlayerBank;
        }
        $("#playerBetOutput"+(parseInt(player_idx)+1)).text("Bet: " + currBet + " $");
        $("#betBTN").attr("disabled",false);
    })

    $(document).on("click","#joinGameBTN",function(){
        if($(this).data('game_uuid')!=null && $("#usernameInput").val()!=""){
            sendWs('joinGame',{game_uuid: $(this).data('game_uuid'), username : $("#usernameInput").val()});
            $("#createGameModal").modal('toggle');
            $(".ingameBTN").show();
        }
    })

    $(document).on("click","#leaveGameBTN",function(){
        leaveGame();
    });

    // local functions
    function leaveGame(){
        sendWs('leaveGame',{game_uuid: currGame, player_uuid : thisPlayer});

        // Resetting frontend
        currBet = 0;
        $(".ingameBTN").hide();
        $(".chipBox").attr("disabled",true);
        $(".chipsBox").css("box-shadow","");
        $("#resetGameBTN").hide();
        $(".resetable").text("");
        $(".chipBox").css("pointer-events","none");
        $("#bankOutput").css("animation","");
        $("#createGameModal").modal('toggle');
    }

    function playerBank(){
        sendWs('playerBank',{game_uuid: currGame, player_uuid : thisPlayer});
    }

    // render functions

    function renderHandInfos(handInfo){
        $("#dealerValue").text("Points: " + handInfo[0].dealer_value);
        $("#dealerOutput").html(handInfo[0].dealer_hand);

        for(var i = 1; i < handInfo.length; i++) {
            $("#playerValue"+i).text("Points: " + handInfo[i].value);
            $("#playerOutput"+i).html(handInfo[i].player_hand);
        }
    }

    function renderPlayerInfos(playerInfo){
        if(thisPlayer==playerInfo[0].curr_player_uuid){
            $("#hitBTN").attr("disabled", false);
            $("#standBTN").attr("disabled", false);
        } else {
            $("#hitBTN").attr("disabled", true);
            $("#standBTN").attr("disabled", true);
        }

        playerInfo.shift();

        for(var i = 0; i < 3; i++) {
            if(i<playerInfo.length){
                if(thisPlayer==playerInfo[i].player_uuid){player_idx=i;}
                $("#playerName"+(i+1)).text(playerInfo[i].name);
                $("#playerBetOutput"+(i+1)).text("Bet: " + playerInfo[i].bet_value + " $");
            } else { // If player quits
                $("#playerName"+(i+1)).text("");
                $("#playerValue"+(i+1)).text("");
                $("#playerBetOutput"+(i+1)).text("");
                $("#playerOutput"+(i+1)).text("");
            }
        }
    }

});